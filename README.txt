
-- SUMMARY --

Extends the Access restriction section in views UI by allowing you to
choose from organic group roles.

Bug reports, feature suggestions and latest developments:
  http://drupal.org/project/issues/2394985


-- INSTALLATION --

1. Copy the VAOGR module to your modules directory and enable it 
   on the Modules page (admin/modules).

-- CREDITS --

Designed and Developed by @edutrul
